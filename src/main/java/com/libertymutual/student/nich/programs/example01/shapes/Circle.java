package com.libertymutual.student.nich.programs.example01.shapes;

import java.awt.Color;
import java.math.BigDecimal;

/*
 * Circle class defines Circle shape
 */
public class Circle extends Shape {
    /**
     * radius used for calculating area
     */
    private int radius;

    public Circle(int radius, final Color color) {
        super(color);
        this.radius = radius;
    }

    @Override
    public BigDecimal getArea() {
        final double pi = 3.14;
        double area = pi * radius * radius;
        return new BigDecimal(area);
    }
}