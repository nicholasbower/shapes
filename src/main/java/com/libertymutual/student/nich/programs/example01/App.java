package com.libertymutual.student.nich.programs.example01;
import java.awt.*;
import com.libertymutual.student.nich.programs.example01.shapes.Square;
import com.libertymutual.student.nich.programs.example01.shapes.Circle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world!
 *
 */
public class App {

    private static final Logger appLogger = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        appLogger.info(App.class + " starting ");
        final int radius = 10;
        Circle circle = new Circle(radius, Color.BLACK);
        System.out.println(circle.getArea());

        final int length = 100;
        Square square = new Square(length, Color.WHITE);
        System.out.println(square.getArea());
    }
}
